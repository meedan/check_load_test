import os
from locust import HttpLocust, TaskSet, task


class GraphqlBehavior(TaskSet):
    # @task(1)
    # def query(self):
    #     headers = {'X-Check-Token': os.getenv('API_KEY', '')}
    #     data = 'query=query{me{current_team{id dbid name}}}'
    #     self.client.post("/api/graphql/", data=data, headers=headers)

    @task(1)
    def index(self):
        #

        headers = {'X-Check-Token': os.getenv('API_KEY', '')}
        with open('./query.gql', 'r') as content_file:
            content = content_file.read()
        self.client.post("/api/graphql/", data=content, headers=headers)




class WebsiteUser(HttpLocust):
    task_set = GraphqlBehavior
    min_wait = 5000
    max_wait = 9000
