FROM python:3

RUN mkdir /var/www
COPY ./ /var/www
RUN pip install locustio
COPY ./start /start
RUN chmod a+x /start
WORKDIR /var/www
EXPOSE 8089
CMD ["/start"]

